import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.util.JiraHome

def jiraHome = ComponentAccessor.getComponent(JiraHome)
def optionsManager = ComponentAccessor.optionsManager
def customFieldManager = ComponentAccessor.customFieldManager
def issueManager = ComponentAccessor.issueManager

log.warn("---------------------- Started option service --------------")

//This expect the file to bew placed inside a "files" folder placed into the home jira folder, also the file shuld be called test.txt but you should change this
def parsedFile = new File(jiraHome.localHomePath + "/files/beta1-channels.txt").getText("UTF-8")
//To extract all the possible values into the file i split the string where there is a newline command
def textOptionsList = parsedFile.split("\n")
textOptionsList.each {
    log.warn(it)
}

def issue = issueManager.getIssueObject("SRF-59") //An issue with the custom field selectable
//This expect the custom field to be named "testSelect", you should change this
def customField = customFieldManager.getCustomFieldObjectByName("Beta Channels")
def fieldConfig = customField.getRelevantConfig(issue)
def existingOptions = optionsManager.getOptions(fieldConfig)

def highestSeq = existingOptions ? existingOptions*.sequence.max() : -1
highestSeq ++

//Iterating the text file, here we create new option if not present or we re-enable previoulsy disabled options
textOptionsList.each { textOptionFromFile ->
    def existingOption = existingOptions.find {
        it.value.trim() == textOptionFromFile.trim()
    }
    //If the option is not present into the existing options i create a new option
    if(existingOption == null){
        optionsManager.createOption(fieldConfig, null as Long, highestSeq, (textOptionFromFile as String).trim())
        highestSeq++
    }
    else{
        //If the option exist but has been disabled, we re-enable it
        if(existingOption.disabled){
            optionsManager.enableOption(existingOption)
        }
    }
}

//getting the newly updated options in case there was a change
def updatedOptions = optionsManager.getOptions(fieldConfig)
//Iterating the options inside the select field, here we are checking if the options are still valid, if not we disable them
updatedOptions.each { existingOption ->
    //find if the existing option is still inside the file
    def existingOptionPresentInsideFile = textOptionsList.find {
        it.trim() == existingOption.value.trim()
    }
    if(existingOptionPresentInsideFile == null){
        //If the option inside the select field has been removed from the file, then i disabled it (theoretically you should be able to still see
        // the value inside issue but it's not gonna be selectable anymore)
        optionsManager.disableOption(existingOption)
    }
}

def sortOptions = optionsManager.getOptions(fieldConfig)
sortOptions.sortOptionsByValue(null)

log.warn("------- End option service ----------------")

