import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.util.JiraHome

def jiraHome = ComponentAccessor.getComponent(JiraHome)
def optionsManager = ComponentAccessor.optionsManager
def customFieldManager = ComponentAccessor.customFieldManager
def issueManager = ComponentAccessor.issueManager

log.warn("---------------------- Started option service --------------")

//This expect the file to bew placed inside a "files" folder placed into the home jira folder, also the file shuld be called test.txt but you should change this
def parsedFile = new File(jiraHome.localHomePath + "/files/beta-subpacks.txt").getText("UTF-8")
//To extract all the possible values into the file i split the string where there is a newline command
def textOptionsList = parsedFile.split("\n")
textOptionsList.each {
    log.warn(it)
}

def issue = issueManager.getIssueObject("SRF-1") //An issue with the custom field selectable
//This expect the custom field to be named "testSelect", you should change this
def customField = customFieldManager.getCustomFieldObjectByName("Beta Subpacks")
def fieldConfig = customField.getRelevantConfig(issue)
def existingOptions = optionsManager.getOptions(fieldConfig)

def highestSeq = existingOptions ? existingOptions*.sequence.max() : -1
highestSeq ++

textOptionsList.each { textOptionFromFile ->
    def numberOfAlreadyExistinOptionsWithSameName = existingOptions.find {
        it.value == textOptionFromFile
    }.collect().size()
    //If the option is not present into the existing options i create a new option
    if(numberOfAlreadyExistinOptionsWithSameName <= 0){
        optionsManager.createOption(fieldConfig, null as Long, highestSeq, (textOptionFromFile as String).trim())
        highestSeq++
    }
}

log.warn("------- End option service ----------------")
